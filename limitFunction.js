function limitFunctionCallCount(cb, n) {
    return () => {
        for (let i = 0; i < n; i++) {
          cb();
        }
      };
    }
    function cb() {
        console.log("Hi Deepak");
      }
      
      limitFunctionCallCount(cb, 2)();

// module.exports = limitFunctionCallCount;