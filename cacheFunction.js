function cacheFunction(cb) {

    let cache = {};

    function invokeCache(...args) {

        let argToString = JSON.stringify(args);

        if (cache.hasOwnProperty(argToString)) {

            return cache[argToString];
        }

        else {

            let newArg = cb(...args); 

            cache[argToString] = newArg;

            return newArg;
        }
    }
    return invokeCache;
}
module.exports=cacheFunction;